package levelEditorV3;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class ColorMenu extends Menu {

	private ArrayList<MenuButton> buttonList = new ArrayList<MenuButton>();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ColorMenu(Box c, MyFrame window) {
	//	c.add(new JLabel("Tiles"));
		for (Color options: ColorConstants.colorOptions) {
			ColorButton button = new ColorButton(options, window, this);
			c.add(button);
			buttonList.add(button);

		}
	}

	public ColorMenu(JPanel frame) {
		for (Color options: ColorConstants.colorOptions) {
			Box c = new Box(BoxLayout.X_AXIS);
			c.setMinimumSize(new Dimension(100,35));
			c.setMaximumSize(new Dimension(100,35));
			c.setPreferredSize(new Dimension(100,35));
			ColorButton button = new ColorButton(options);
			c.add(button);
			c.add(new ColorTextField(Character.toString(ColorConstants.getType(options)), 1, options));
			frame.add(c);
			buttonList.add(button);
		}
	}



	public ArrayList<MenuButton> getButtonList() {
		return buttonList;
	}
}
