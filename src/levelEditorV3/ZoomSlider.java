package levelEditorV3;

import java.awt.Dimension;

import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ZoomSlider extends JSlider {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ZoomSlider(int orientation, int min, int max, int val, DrawingArea drawingArea, JScrollPane scroll) {
		super(orientation, min, max, val);
		setPreferredSize(new Dimension(150, 20));
		setMaximumSize(new Dimension(150, 20));
		setMinimumSize(new Dimension(150, 20));

		this.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
					Tile.setTileSize(ZoomSlider.this.getValue());
					drawingArea.setCursorActive(false);
					drawingArea.updateSize();
					scroll.setViewportView(drawingArea);
			}
		});
	}

}
