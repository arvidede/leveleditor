package levelEditorV3;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.border.LineBorder;


public abstract class MenuButton extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final int size = 25;

	public MenuButton(Color cl) {
		super(Character.toString(ColorConstants.getType(cl)));
		this.setMinimumSize(new Dimension(size,size));
		this.setMaximumSize(new Dimension(size,size));
		this.setPreferredSize(new Dimension(size, size));
		this.setBorder(new LineBorder(Color.BLACK));
	}

	public MenuButton() { 
		super();
		this.setMinimumSize(new Dimension(size,size));
		this.setMaximumSize(new Dimension(size,size));
		this.setPreferredSize(new Dimension(size, size));
		this.setBorder(new LineBorder(Color.BLACK));
	}

	public void buttonFrameVisible(Menu menu, MenuButton b) {
		for(MenuButton button : menu.getButtonList()) {
			if(button.getIfChosen()) {
				button.setChosen(false);
				button.setBorder(new LineBorder(Color.BLACK));	
			}
		}
		setChosen(true);

		if(b.getIfChosen()){
			setBorder(new LineBorder(Color.BLACK, 4, false));
		}
	}

	public abstract boolean getIfChosen();

	public abstract void setChosen(Boolean chosen);
	
	public abstract void doThings();


}





