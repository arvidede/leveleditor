package levelEditorV3;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

public class InputButton extends JMenuItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InputButton (MyFrame myFrame, ColorMenu tiles) {
		super("Set Output");
		setToolTipText("Change the output character for each tile");
		setAccelerator(KeyStroke.getKeyStroke('M', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFrame choiceFrame = new JFrame("Tiles");
//				JMenuBar bar = new JMenuBar();
//				bar.setOpaque(false);
//				choiceFrame.add(bar);
//				bar.add(new ExitButton(choiceFrame, 'O'));
				choiceFrame.setSize(new Dimension(300, 650));
				choiceFrame.setVisible(true);
				choiceFrame.setLocationRelativeTo(myFrame);
				choiceFrame.setAlwaysOnTop(true);
				//				c.setVisible(true);
				JPanel panel = new JPanel();
				panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

				panel.setMinimumSize(new Dimension(8, 800));
				panel.setMaximumSize(new Dimension(8, 800));
				panel.setPreferredSize(new Dimension(8, 800));
				//choiceFrame.add(panel);
				panel.add(Box.createVerticalStrut(30));
				new ColorMenu(panel);
				JButton ok = new JButton("OK");
				panel.add(ok);
				choiceFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
				
				JScrollPane scroll = new JScrollPane(panel);
				choiceFrame.getContentPane().add(scroll, BorderLayout.CENTER);
				scroll.getVerticalScrollBar().setUnitIncrement(10);
				
				ok.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						choiceFrame.dispose();
						for (MenuButton b : tiles.getButtonList() ) {
							b.doThings();
						}
					}

				});
			}
		});

	}

}