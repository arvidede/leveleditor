package levelEditorV3;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class ClearButton extends JMenuItem{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ClearButton(String name, final DrawingArea drawingArea) {

		super(name);

		setAccelerator(KeyStroke.getKeyStroke('W', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));

		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
//				drawingArea.getList().clear();
				drawingArea.saveList();
				drawingArea.repaint();
			}

		});
	}

}
