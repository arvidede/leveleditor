package levelEditorV3;

import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class ColorTextField extends JTextField {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ColorTextField(String input, int length, Color cl)  {
		super(input, length);

		this.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent e)  {
				ColorConstants.setOutput(cl, getText().charAt(0));
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

}
