package levelEditorV3;

import javax.swing.*;

import java.awt.*;
import java.util.ArrayList;

public class DrawingArea extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static int tileSize; 
	private static int vertTiles;
	private static int horTiles;
	private int currentX;
	private int currentY;
	private int boxStartX;
	private int boxStartY;
	private int boxEndX;
	private int boxEndY;
	private boolean cursorActive = false;
	private boolean saved;// = true;

	private Color cl = Color.BLACK;

	private Model model = new Model();

	public DrawingArea(int width, int height) {
		vertTiles = width;
		horTiles = height;
		tileSize = Tile.getTileSize(); 
		setMinimumSize(new Dimension(vertTiles * tileSize, horTiles * tileSize));
		setMaximumSize(new Dimension(vertTiles * tileSize, horTiles * tileSize));
		setPreferredSize(new Dimension(vertTiles * tileSize, horTiles * tileSize));
	}

	public void printObject(int x, int y, Color cl) {
		model.setObject(convertToGrid(x), convertToGrid(y), cl);
		saved = false;
	}

	public void saved() {
		saved = true;
	}

	public boolean isSaved()  {
		if(model.getList().size() > 0)  {
			return saved;
		} else {
			return true;
		}	
	}

	public void drawGrid(Graphics g) {
		g.setColor(Color.WHITE);

		for(int i = 1; i < vertTiles; i++) {
			g.drawLine(tileSize * i, 0, tileSize * i, tileSize * horTiles); //vertical
		}

		for(int i = 1; i < horTiles; i++) {
			g.drawLine(0, tileSize * i, tileSize * vertTiles, tileSize * i);  //horizontal
		}

		g.setColor(Color.LIGHT_GRAY);
		g.drawLine(tileSize * vertTiles, 0, tileSize * vertTiles, tileSize * horTiles);
		g.drawLine(0, tileSize * horTiles, tileSize * vertTiles, tileSize * horTiles);

	}

	public int convertToGrid(int coord) {
		return (int) Math.floor(coord / tileSize);
	}

	public int getWinWidth() {
		return tileSize * vertTiles;
	}

	public int getWinHeight() {
		return tileSize * horTiles;
	}

	public void boxStart(int x, int y) {
		boxStartX = convertToGrid(x) * tileSize;
		boxStartY = convertToGrid(y) * tileSize;
	}

	public void boxEnd(int x, int y) {
		boxEndX = convertToGrid(x) *tileSize;
		boxEndY = convertToGrid(y) * tileSize;
	}

	public void drawBox(Graphics g)  {
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.setColor(Color.LIGHT_GRAY);
		Stroke dashed = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
		g2d.setStroke(dashed);
	
		if (boxStartY < boxEndY && boxStartX < boxEndX)  { //down right
			g2d.drawRect(boxStartX, boxStartY, Math.abs(boxStartX -  boxEndX), Math.abs(boxStartY - boxEndY));
		} else if (boxStartX > boxEndX && boxStartY < boxEndY) { // down left
			g2d.drawRect(boxEndX, boxStartY, Math.abs(boxStartX -  boxEndX), Math.abs(boxStartY - boxEndY));
		} else if (boxStartX > boxEndX && boxStartY  > boxEndY) { // up left
			g2d.drawRect(boxEndX, boxEndY, Math.abs(boxStartX -  boxEndX), Math.abs(boxStartY - boxEndY));
		} else if (boxStartX < boxEndX && boxStartY  > boxEndY) { // up right
			g2d.drawRect(boxStartX, boxEndY, Math.abs(boxStartX -  boxEndX), Math.abs(boxStartY - boxEndY));
		}
		
		g2d.dispose();
	}

	public void setCurrent (int x, int y, Color cl) {
		currentX = (int) (Math.floor(x / tileSize) * tileSize);
		currentY = (int) (Math.floor(y / tileSize) * tileSize);
		cursorActive = true;
		this.cl = cl;
	}

	public Model getModel() {
		return model;
	}

	public ArrayList<Tile> getList() {
		return model.getList();
	}
	
	public void saveList() {
		model.saveList();
	}

	@Override
	public void paintComponent(Graphics g) {
		tileSize = Tile.getTileSize();   // lol what

		for (Tile sq : model.getList()) {
			sq.paintYourself(g);
		}

		if(cursorActive) {
			g.setColor(cl);
			g.drawRect(currentX+1, currentY+1, tileSize-2, tileSize-2);
		}
		drawGrid(g);

		if(boxStartX != 0 && boxStartY != 0) {
			drawBox(g);
		}

	}

	public void reset() {
		model.reset();
	}
	
	public void setNewSize(int width, int height) {
		vertTiles = width;
		horTiles = height;
		setMinimumSize(new Dimension(vertTiles*tileSize, horTiles*tileSize));
		setMaximumSize(new Dimension(vertTiles*tileSize, horTiles*tileSize));
		setPreferredSize(new Dimension(vertTiles*tileSize, horTiles*tileSize));
		repaint();
	}

	public void updateSize() {
		setMinimumSize(new Dimension(vertTiles*tileSize, horTiles*tileSize));
		setMaximumSize(new Dimension(vertTiles*tileSize, horTiles*tileSize));
		setPreferredSize(new Dimension(vertTiles*tileSize, horTiles*tileSize));
		repaint();
	}

	public void setCursorActive(boolean cursorActive) {
		this.cursorActive = cursorActive;
	}

	public int getTileWidth() {
		return vertTiles;
	}

	public int getTileHeight() {
		return horTiles;
	}

	public void endAction() {
		model.endAction();
	}

	public void setPlayerPos(int x, int y) {
		model.setObject(x, y, ColorConstants.player);
	}
}
