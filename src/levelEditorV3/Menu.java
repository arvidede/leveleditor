package levelEditorV3;


import java.util.ArrayList;

import javax.swing.JComponent;

public abstract class Menu extends JComponent {

//Komplettering: Programmet har errors - det går inte att köra. 
//Kolla på om ni har filnamnen och Klassnamnen samma.  


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Menu(){
		
	}

	public abstract ArrayList<MenuButton> getButtonList();
}
