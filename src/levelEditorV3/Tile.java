package levelEditorV3;

import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;

public class Tile implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int x;
	private int y;
	private static int tileSize = 15;
	private Color cl;

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public Color getColor() {
		return cl;
	}

	public Tile(int x, int y, Color cl) {
		this.x = x;
		this.y = y;
		this.cl = cl;
	}

	protected void paintYourself(Graphics g) {
		g.setColor(cl);
		g.fillRect(x*tileSize, y*tileSize, tileSize, tileSize);
	}

	public static int getTileSize() {
		return tileSize;
	}
	
	public static void setTileSize(int sz) {
		tileSize = sz;
	}
	
}
