package levelEditorV3;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class UndoButton extends JMenuItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UndoButton(String name, DrawingArea drawingArea) {
		super(name);
		setAccelerator(KeyStroke.getKeyStroke('Z', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				drawingArea.getModel().undo();
				drawingArea.repaint();
			}
		});

	}
}
