package levelEditorV3;

/**
 * @author Arvid Edenheim
 * *************************************************************
 * Simple level editor for 2D-sidescroller game
 * Written as a project for LiU course TDDE10
 * 2017 - 02 - 24
 * Outputs txt-file on the format: x,y,tiletype
 * First row starts with: mapwidth, mapheight, playertile
 * *************************************************************
 * 
 * Remaining issues: 
 * - Same background window and map area (JFrame will be the cause of my death)
 * - Undo after clearing the window requires double pressing the button followed by pressing "Do". What
 * 
 * TODO:
 * - Undo/Do  **done**
 * - Copy (implies marking a region and being able to move/copy the tiles in it)
 * - Check close without saving **done**
 * - Connect tile button to custom output **done**
 * - Replace
 * - Fill
 * - Toolbar
 */

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Main {

	public static void main(String[] args) {
		greetWithPopup();
	}

	private static void greetWithPopup() {
		JTextField height = new JTextField("20");
		JTextField width = new JTextField("100");

		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(false);

		JPanel panel = new JPanel(new GridLayout(2, 2));

		panel.add(new JLabel("Height") );
		panel.add(height);
		panel.add(new JLabel("Width"));
		panel.add(width);

		try {
			int output =	JOptionPane.showConfirmDialog(
					frame,
					panel,
					"Map Size", 
					JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.PLAIN_MESSAGE);

			//			System.out.println(Integer.valueOf(height.getText()) + " " + Integer.valueOf(width.getText()));
			if (output == 0)  {	
				new MyFrame(Integer.valueOf(width.getText()), Integer.valueOf(height.getText()));
			} else {
				System.exit(0);
			}

		} catch (NumberFormatException e) {
			//			new MyFrame(100, 20);							//quick start while editing
			System.exit(0);
		}
	}

}
