package levelEditorV3;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class SavePngButton extends JMenuItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public SavePngButton(String name, final DrawingArea drawingArea, MyFrame myFrame) {
		super(name);
		setAccelerator(KeyStroke.getKeyStroke('P', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {		

				JFileChooser chooser = new JFileChooser();
				if(chooser.showSaveDialog(myFrame) == JFileChooser.APPROVE_OPTION) {
					File outputFile = chooser.getSelectedFile();				
					save(outputFile);
				} 
			}

			private void save(File outputFile) {
				BufferedImage img = new BufferedImage(drawingArea.getWidth(), drawingArea.getHeight(), BufferedImage.TYPE_INT_ARGB);
				Graphics g = img.getGraphics();
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, drawingArea.getWidth(), drawingArea.getHeight());
				drawingArea.paintComponent(g);
				try {
					ImageIO.write(img, "png", outputFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
				drawingArea.saved();
			}

		});
	}
}
