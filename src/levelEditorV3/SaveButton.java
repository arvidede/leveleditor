package levelEditorV3;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class SaveButton extends JMenuItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SaveButton(String name, final DrawingArea drawingArea, MyFrame myFrame) {
		super(name);
		setToolTipText("Save as .txt-file");
		setAccelerator(KeyStroke.getKeyStroke('S', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setSelectedFile(new File("new map"));
				chooser.setCurrentDirectory(new java.io.File("."));
				if(chooser.showSaveDialog(myFrame) == JFileChooser.APPROVE_OPTION) {
					File outputFile = chooser.getSelectedFile();				
					save(outputFile, drawingArea.getList());
				}
			}
			
			private void save(File outputFile, ArrayList<Tile> myList) {
				try {
					BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile + ".txt", true));
					writer.write(Integer.toString(drawingArea.getTileWidth()) + ',' + Integer.toString(drawingArea.getTileHeight()) + ',');
					
					for(Tile sq : myList) {
						writer.write(Integer.toString(sq.getX()) + 
								',' + Integer.toString(sq.getY()) + ',' +  
								ColorConstants.getType(sq.getColor()));
						writer.newLine();
					}
					writer.close();
				} catch (IOException e) {
					System.out.println("Fel vid skrivning till fil");
					e.printStackTrace();
				}
				drawingArea.saved();
			}
		});
	}
}
