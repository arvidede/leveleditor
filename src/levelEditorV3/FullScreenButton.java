package levelEditorV3;

import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class FullScreenButton extends JMenuItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean fullScreen = false;

	public FullScreenButton(String name, MyFrame frame, GraphicsDevice gd) {
		super(name);

		setAccelerator(KeyStroke.getKeyStroke('F', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));

		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				 if (gd.isFullScreenSupported() && !fullScreen) {
				        gd.setFullScreenWindow(frame);
				        fullScreen = true;
				   } else {
					   gd.setDisplayMode(new DisplayMode(600, 600, 8, gd.getDisplayMode().getRefreshRate()));
					   fullScreen = false;
				   }
			}

		});
	}
}
