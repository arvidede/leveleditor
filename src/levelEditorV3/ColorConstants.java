package levelEditorV3;

import java.awt.Color;

/**
 * Most horrible piece of code ever written. Do not read.
 * Should have been done using hashmaps. 
 * H = Player, can only be placed once.
 * 
 */

public class ColorConstants {
	
	
	public static final Color gray = new Color(100, 100, 100);
	public static final Color green = new Color(3, 119, 73);
	public static final Color player = new Color(139, 69, 19);
	public static final Color coin = new Color(153, 0, 153);
	public static final Color castle = new Color(20, 20, 20);
	public static final Color bug = new Color(79, 250, 45);
	public static final Color flower = new Color(200, 40, 63);
	public static final Color heart = new Color(239, 65, 65);
	public static char i = 'i';
	public static char j = 'j';
	public static char k= 'k';
	public static char a = 'a';
	public static char v = 'v';
	public static char o = 'o';
	public static char p = 'p';
	public static char w = 'w';
	public static char g = 'g';
	public static char b = 'b';
	public static char c = 'c';
	public static char d = 'd';
	public static char r = 'r';
	public static char playr = 'H';
	public static char cn = 'z';
	public static char bugc = 'y';
	public static char flowerc = 'q';
	public static char castleChar = 'u';
	public static char hrt = 'f';
	public static char NA = 'x';

	public static final Color colorOptions[] = {
			Color.BLACK,
			Color.BLUE,
			Color.MAGENTA,
			Color.RED,
			Color.CYAN,
			Color.WHITE,
			Color.GREEN,
			Color.YELLOW,
			Color.ORANGE,
			Color.PINK,
			gray,
			green,
			Color.LIGHT_GRAY,
			Color.DARK_GRAY,
			player,
			coin,
			castle,
			bug,
			flower,
			heart
	};

	public ColorConstants() {
		
	}

	public static Color convertType(char c) {

		if(c == 'j') {
			return Color.BLACK;
		} else if (c == 'i') {
			return Color.RED;
		} else if (c == 'k') {
			return Color.BLUE;
		} else if (c == 'g') {
			return Color.GREEN;
		} else if (c == 'a') {
			return Color.ORANGE;
		} else if (c == 'b') {
			return Color.CYAN;
		} else if (c == 'c' ) {
			return Color.MAGENTA;
		} else if(c == 'd') {
			return Color.PINK;
		} else if (c == 'r') {
			return Color.LIGHT_GRAY;
		} else if (c == 'v') {
			return Color.DARK_GRAY;
		} else if (c ==  'p') {
			return gray;
		} else if (c == 'w') {
			return Color.YELLOW;
		} else if (c == 'o') {
			return green;
		} else if (c == playr) {
			return player;
		} else if(c == cn) {
			return coin;
		} else if(c == castleChar) {
			return castle;
		} else if(c == bugc) {
			return bug; 
		} else if(c == flowerc) {
				return flower;
		} else if(c == hrt) {
			return heart;
		}
		return Color.WHITE;
	}

	public static char getType(Color cl) {
		if(cl == Color.BLACK) {
			return j;
		} else if (cl == Color.RED) {
			return i;
		} else if (cl == Color.BLUE) {
			return k;
		} else if (cl == Color.GREEN) {
			return  g;
		} else if (cl == Color.ORANGE) {
			return a;
		} else if (cl == Color.CYAN) {
			return b;
		} else if (cl == Color.MAGENTA) {
			return c;
		} else if(cl == Color.PINK) {
			return d;
		} else if (cl == Color.LIGHT_GRAY) {
			return r;
		} else if (cl == Color.DARK_GRAY) {
			return v;
		} else if (cl == gray) {
			return p;
		} else if (cl == Color.YELLOW) {
			return w;
		} else if (cl == green) {
			return o;
		} else if(cl == player) {
			return playr;
		} else if (cl == coin) {
			return cn;
		} else if (cl == castle) {
			return castleChar;
		} else if (cl == flower) {
			return flowerc;
		} else if (cl == bug) {
			return bugc;
		} else if(cl == heart) {
			return hrt;
		}
		return ' ';
	}

	public static void setOutput(Color cl, char c)  {

		if(cl == Color.BLACK) {
			j = c;
		} else if (cl == Color.RED) {
			i = c;
		} else if (cl == Color.BLUE) {
			k = c;
		} else if (cl == Color.GREEN) {
			g = c;
		} else if (cl == Color.ORANGE) {
			a = c;
		} else if (cl == Color.CYAN) {
			b = c;
		} else if (cl == Color.MAGENTA) {
			ColorConstants.c = c;
		} else if(cl == Color.PINK) {
			d = c;
		} else if (cl == Color.LIGHT_GRAY) {
			r = c;
		} else if (cl == Color.DARK_GRAY) {
			v = c;
		} else if (cl == gray) {
			p = c;
		} else if (cl == Color.YELLOW) {
			w = c;
		} else if (cl == green) {
			o = c;
		} else if (cl == player) {
			playr = c;
		} else if(cl == coin) {
			cn = c;
		} else if(cl == castle) {
			castleChar = c;
		}
	}


}
