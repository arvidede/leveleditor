package levelEditorV3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JMenuItem;

public class OpenButton extends JMenuItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private DrawingArea drawingArea;

	public OpenButton(String name, final DrawingArea drawingArea, MyFrame myFrame) {
		super(name);
		this .drawingArea = drawingArea;
		setToolTipText("Öppna filer av format .txt");
		
		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {		
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				if(chooser.showOpenDialog(myFrame) == JFileChooser.APPROVE_OPTION) {
					File inputFile = chooser.getSelectedFile();				
					readFile(inputFile);
				} 
			}
		});	
	}
	
	public void readFile(File inputFile) {
		File file = inputFile;
		String current; // = "";
		String split =",";
		int x;
		int y;
		char type;
		String[] ch;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			
			drawingArea.reset();
			
//			drawingArea.getModel().getList().clear();
			
			current = reader.readLine();
			ch = current.split(split);
			
			drawingArea.setNewSize(Integer.parseInt(ch[0]), Integer.parseInt(ch[1]));
			drawingArea.setPlayerPos(Integer.parseInt(ch[2]), Integer.parseInt(ch[3]));
			
			
			while((current = reader.readLine()) != null) {
				ch = current.split(split);
				x = Integer.parseInt(ch[0]);
				y = Integer.parseInt(ch[1]);
				type =  ch[2].charAt(0);
				drawingArea.getModel().getList().add(new Tile(x,y,ColorConstants.convertType(type)));
			}
			
			drawingArea.repaint();
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
