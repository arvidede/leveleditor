package levelEditorV3;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;

public class Model{
	private ArrayList<Tile> printList = new ArrayList<Tile>();
	private ArrayList<Tile> recentlyAdded = new ArrayList<Tile>();
	private ArrayList<ArrayList<Tile>> savedAction = new ArrayList<ArrayList<Tile>>();
	private ArrayList<ArrayList<Tile>> removedList = new ArrayList<ArrayList<Tile>>();

	public Model() {
	}

	public void setObject(int x, int y, Color cl) {
		removedList.clear();
			
		if(cl == Color.WHITE )  {
			removeObject(x, y);
			// get(x,y), lägg det i savedAction, spara sedan i removedList vid knappslut
		} else {
			Tile toAdd = new Tile(x,y,cl);
			recentlyAdded.add(toAdd);
			
			if (cl != ColorConstants.player) {			
				if(contains(x,y)) {
					removeObject(x, y);	
				} 
				printList.add(toAdd);
			} else  if (contains(x,y))  {
				if (printList.size() != 0  && printList.get(0).getColor() == ColorConstants.player ) {
					printList.remove(0);
				}
				removeObject(x, y);
				printList.add(0, toAdd);
			} else  if (printList.size() != 0  && printList.get(0).getColor() == ColorConstants.player) {
				printList.remove(0);
				printList.add(0, toAdd);
			} else {
				printList.add(toAdd);
			}
		}
	}

	private boolean contains(int x, int y) {
		for(Tile sq : printList) {
			if (x == sq.getX() && y == sq.getY()) {
				return true;
			}
		}
		return false;
	}
	
	public void saveList() {
		removedList.add(printList);
		printList.clear();
	}

	public void endAction() {
		savedAction.add(recentlyAdded);
		recentlyAdded = new ArrayList<Tile>();
	}
	
	public void reset() {
		savedAction = new ArrayList<ArrayList<Tile>>();
		removedList = new ArrayList<ArrayList<Tile>>();
		printList = new ArrayList<Tile>();
		recentlyAdded = new ArrayList<Tile>();	
	}

	public void undo() {
		if (savedAction.size() > 0) {
			printList.removeAll(savedAction.get(savedAction.size() -1));
			removedList.add(savedAction.get(savedAction.size() -1));
			savedAction.remove(savedAction.size() - 1);
			savedAction.trimToSize();
		}
	}

	public void redo() {
		if (removedList.size() > 0) {
			printList.addAll(removedList.get(removedList.size()-1));
			savedAction.add(removedList.get(removedList.size() -1));
			removedList.remove(removedList.size()-1);
			removedList.trimToSize();
		}
	}

	public void removeObject(int x, int y) {
		Iterator<Tile> iter = printList.iterator();
		while (iter.hasNext()) {
			Tile t = iter.next();
			if (x == t.getX() && y == t.getY())
				iter.remove();
		}
	}

	public void setArrayList(ArrayList<Tile> model) {
		printList = model;
	}

	public ArrayList<Tile> getList() {
		return printList;
	}

}
