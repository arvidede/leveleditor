package levelEditorV3;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public class ExitButton extends JMenuItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExitButton(MyFrame frame) {
		super("Exit");

		setAccelerator(KeyStroke.getKeyStroke('X', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
					frame.close();
			}
		}); 
	}

}
