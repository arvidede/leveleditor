package levelEditorV3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;

public class MyFrame extends JFrame {
	private Color currentColor = Color.BLACK;
	private DrawingArea drawingArea;
	private SaveButton save;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyFrame(int width, int height) {

		super("Level Editor V.3");
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		pack();
		setSize(screenSize.width/2,screenSize.height/2);
		//		setSize(new Dimension(width*Tile.getTileSize(), height*Tile.getTileSize()));

		drawingArea = new DrawingArea(width, height);
		JMenuBar menuBar = new JMenuBar();
		JMenuBar topMenu = new JMenuBar();
		JMenu files = new JMenu("Files");
		JMenu edit = new JMenu("Edit");
//		JPanel sidePanel = new JPanel();
		Box coord = new Box(BoxLayout.X_AXIS);
		JLabel xy = new JLabel(" ");
		JScrollPane scroll = new JScrollPane(drawingArea); 
		JSlider zoomSlider = new ZoomSlider(JSlider.HORIZONTAL, 5, 35, 15, drawingArea, scroll);
		Box c = new Box(BoxLayout.X_AXIS);
		ColorMenu tiles = new ColorMenu(c, this);

		edit.setBackground(Color.black);

		coord.add(xy);

		this.add(menuBar, BorderLayout.SOUTH);
		this.add(topMenu, BorderLayout.NORTH);
//		this.add(sidePanel, BorderLayout.EAST);

		topMenu.add(files);
		topMenu.add(edit);

		save = new SaveButton("Save as txt", drawingArea, this);
		edit.add(new ClearButton("Clear", drawingArea));
		files.add(new OpenButton("Open", drawingArea, this));
		files.add(new SavePngButton("Save as png", drawingArea, this));
		files.add(save);
		edit.add(new UndoButton("Undo", drawingArea));
		edit.add(new RedoButton("Redo", drawingArea));
		files.add(new ExitButton(this));

		menuBar.add(Box.createHorizontalStrut(10));
		menuBar.add(coord);
		menuBar.add(Box.createHorizontalGlue());
		menuBar.add(zoomSlider, BorderLayout.EAST);

		menuBar.setBorder(new BevelBorder(BevelBorder.RAISED));
//		sidePanel.setBorder(new BevelBorder(BevelBorder.RAISED));
		topMenu.add(Box.createHorizontalStrut(50));
		topMenu.add(c);

		c.add(tiles);
		edit.add(new InputButton(this, tiles));

//		sidePanel.setMinimumSize(new Dimension(40,0));
//		sidePanel.setMaximumSize(new Dimension(40,0));
//		sidePanel.setPreferredSize(new Dimension(40, 0));

		getContentPane().add(scroll, BorderLayout.CENTER);
		scroll.getHorizontalScrollBar().setUnitIncrement(10);
		scroll.getVerticalScrollBar().setUnitIncrement(10);

		 GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		 
		edit.add(new FullScreenButton("Toggle Fullscreen", this, gd));
		edit.add(new JLabel("H = Player, can only be placed once"));
		 
		setVisible(true);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		this.addWindowListener( new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});

		drawingArea.addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseDragged(MouseEvent e) {

				if(e.getX() < drawingArea.getWinWidth() && e.getY() < drawingArea.getWinHeight() 
						&& e.getX() > 0  && e.getY() > 0 ) {
					if(SwingUtilities.isLeftMouseButton(e))  {
						drawingArea.printObject(e.getX(), e.getY(), currentColor);
						xy.setText("(" + Integer.toString(drawingArea.convertToGrid(e.getX())) + ", "
								+ Integer.toString(drawingArea.convertToGrid(e.getY())) + ")");
						drawingArea.setCurrent(e.getX(), e.getY(), currentColor);
						repaint();
					} else if(SwingUtilities.isRightMouseButton(e)) {
						drawingArea.boxEnd(e.getX(), e.getY());
						repaint();
					}
				}
			}


			@Override
			public void mouseMoved(MouseEvent e) {
				if(e.getX() < drawingArea.getWinWidth() && e.getY() < drawingArea.getWinHeight() 
						&& e.getX() > 0  && e.getY() > 0) {

					//					setCursor(MyFrame.this.getToolkit().createCustomCursor(
					//				            new BufferedImage(3, 3, BufferedImage.TYPE_INT_ARGB), new Point(0, 0),
					//				            "null"));
					//					
					xy.setVisible(true);
					drawingArea.setCurrent(e.getX(), e.getY(), currentColor);
					xy.setText("(" + Integer.toString(drawingArea.convertToGrid(e.getX())) + ", "
							+ Integer.toString(drawingArea.convertToGrid(e.getY())) + ")");
					repaint();
				} else {

					//					setCursor(Cursor.getDefaultCursor());

					drawingArea.setCursorActive(false);
					xy.setVisible(false);
					drawingArea.repaint();
				}
			}

		});

		drawingArea.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub	
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				if(e.getX() < drawingArea.getWinWidth() && e.getY() <= drawingArea.getWinHeight() 
						&& e.getX() > 0  && e.getY()  > 0) {  
					if(SwingUtilities.isLeftMouseButton(e))  {
						drawingArea.boxStart(0, 0);
						drawingArea.printObject(e.getX(), e.getY(), currentColor);
						repaint();
					} else if (SwingUtilities.isRightMouseButton(e)) {
						drawingArea.boxStart(e.getX(), e.getY());
					}
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			
				if(SwingUtilities.isLeftMouseButton(e))  {
					drawingArea.endAction();
				} else if (SwingUtilities.isRightMouseButton(e)) {
					//setCursor(Cursor.HAND_CURSOR);
				}
			}

		});
	}

	public void setColor(Color myColor) {
		currentColor = myColor;
	}

	public void close() {
		if(!drawingArea.isSaved()) {
			int confirm = JOptionPane.showOptionDialog(
					null, "Map is not saved. Close without saving?", 
					"Exit Confirmation", JOptionPane.YES_NO_OPTION, 
					JOptionPane.QUESTION_MESSAGE, null, null, null);
			if (confirm == 0) {
				System.exit(0);
			} else {
				save.doClick();
			}
		} else {
			System.exit(0);
		}
	}

}
