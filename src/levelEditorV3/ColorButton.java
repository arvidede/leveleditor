package levelEditorV3;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ColorButton extends MenuButton {

	private Color myColor;
	private MyFrame myWindow;
	private Boolean chosenColor = false;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ColorButton(Color cl, MyFrame window, final ColorMenu menu) {
		super(cl);
		myColor = cl;
		myWindow = window;
		this.setBackground(cl);
		this.setOpaque(true); 				//visible color button

		this.addActionListener(new ActionListener()  {
			@Override
			public void actionPerformed(ActionEvent e) {
				myWindow.setColor(myColor);
				ColorButton.super.buttonFrameVisible(menu, ColorButton.this);
			}
		});
	}
	
	public ColorButton(Color cl) {
		super();
		myColor = cl;
		this.setBackground(cl);
		this.setOpaque(true); 		
	}

	@Override
	public boolean getIfChosen() {
		return chosenColor;
	}

	@Override
	public void setChosen(Boolean chosen) {
		chosenColor = chosen;
	}

	@Override
	public void doThings() {
		setText(Character.toString(ColorConstants.getType(myColor)));
	}
}
